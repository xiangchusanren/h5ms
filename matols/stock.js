//MS量化交易 h5 canvas绘制K线图标的JS组件
var stock={
	color:{
		border:"#ccc",//边框颜色
		withinBorder:"#dedede",//内边框颜色
		solid:"#333",//虚线颜色
		trend:"#1879fc",//K线颜色
		fill:"rgba(154,196,248,.3)",//填充颜色
		arc:"#0453b4",	//圆心
		volume:"#999",	//成交量颜色
		red:"#D0011B",
		green:"#60AE07",
		black:"#000",
		purple:"#9012FE",
		mts:"#f6c750",
		minuteAvg:"#ffff00",//分时均线
		avg:{//日K
			_5:"#155a9c", _30:"#f6c750"
		},
		MACD:{ DIF:"#f6c750", DEA:"#155a9c" }
	},
	touch:{left:5, top:100},
	stockID:"",  //当前股票ID,
	stockName:"",//股票名称
	data:null,   //当前行情数据,
	datas:null,  //完整数据 日期0、开1、收2、高3、低4、成交量5、成交额6、MACD7、DIF8、DEA9、EMA5-10、涨跌方向11、涨跌幅12
	macds:null,  //macd策略标记数据
	sinas:null,  //macd三角形态标记点
	cstrs:null,  //缠论k线融合
	clbis:null,  //缠论笔
	tsps:null,   //趋势支撑
	trts:null,   //趋势阻力
	isPC:false,//判断电脑还是手机
	day:null,//今天日期
	addNum:function(num1, num2) {
		var sq1,sq2,m;
		try {sq1 = num1.toString().split(".")[1].length;} catch (e) {sq1 = 0;}
		try {sq2 = num2.toString().split(".")[1].length;} catch (e) {sq2 = 0;}
		m = Math.pow(10,Math.max(sq1, sq2));
		return (num1 * m + num2 * m) / m;
	},
	configData:null,
	//只加载一次
	load:function(data, callback){
		this.stockID=data.stockID;
		this.stockName=data.stockName;
		this.day=data.day;
		this.datas=data.datas;
		this.macds=data.macds;
		this.sinas=data.sinas;
		this.cstrs=data.cstrs;
		this.clbis=data.clbis;
		this.tsps=data.tsps;
		this.trts=data.trts;
		data.default=data.default;
		this.isPC=this.script.isPC();
		if(this.isPC){
			// $("body").css({width:1200,height:670});
		}else{
			$(window).bind('resize load', function() {
				$("body").css("zoom", $(window).width() / 640);
			});
		}
		stock.script.load(data.default);  //引入JS
		callback();
		delete data;
	},
	//加载JS
	script:{
		isPC:function() {
			var userAgentInfo = navigator.userAgent;
			var Agents = ["Android", "iPhone","SymbianOS", "Windows Phone","iPad", "iPod"];
			var flag = true;
			for (var v = 0; v < Agents.length; v++) {
				if (userAgentInfo.indexOf(Agents[v]) > 0) { flag = false; break; }
			}
			return flag;
		},
		reload:function(name){ $("#loading").show();  this.load(name); },
		//matols.com h5 canvas绘制K线图标的JS组件
		load:function(name){
			eval("stock_"+name).load(name,function(result){
				//MS量化交易的MACD指标和K线模板
				if($("#"+name).length==0){
					str="";
					str+='<div class="item item-k" id="'+name+'"><div class="touch" id="touch-'+name+'">';
					
					str+='<div class="K-line"><ul class="price"> <li></li> <li></li><li></li><li></li><li></li></ul> <canvas id="'+name+'-k" class="k"></canvas></div>';
					str+='<ul class="KL-time"><li></li> <li></li><li></li><li></li><li></li></ul>';
					
					str+='<div class="L-line"><ul class="volume"><li class="max-volume"></li><li></li><li></li></ul><canvas id="'+name+'-l" class="l"></canvas></div>';
					str+='<div class="M-line"><ul class="macds"><li class="max-macds"></li><li></li><li></li></ul><canvas id="'+name+'-m" class="m"></canvas></div>';
					
					str+='<ul class="title"></ul>';
					str+='<ul class="MACD"><li style=" color:#252525">MACD: <span></span></li><li style=" color:'+stock.color.MACD.DIF+'">DIF: <span></span></li><li style="color:'+stock.color.MACD.DEA+'">DEA: <span></span></li></ul>';
					
					str+='<div class="x"><span class="current-price"></span></div><div class="y"></div>';
					str+='<div class="zoom"><span class="up"></span><span class="down"></span></div>';
					
					str+='</div></div><div class="infos" id="infos-'+name+'"><div id="infok"></div></div>';
					$("body").append(str);
				}
				eval("stock_"+name).canvas(function(){  $("#"+name).show(); });   //画图
			});
		}
	}
}

//MS量化交易 h5 canvas绘制K线图标的JS组件
var stock_dayK={
		data:null,
		pagedata:null,
		name:"",
		xy:[],
		define:{ pagesize:150, flexsize:[50,100,150,200,250,300,350,400,450,500,550,600], rightsize:0, interval:0,  ofset:0,endsize:0},//间隔
		//只加载一次
		load:function(name,c,o){
			this.name=name;
			data= stock.datas.substring(0,stock.datas.length-1);
			data=data.split(";");
			stock_dayK.data=data;
			c.call(o,true);
		},
		//画图
		canvas:function(c,o){
			this.dispose();//先处理数据
			this.k.load();     //绘制K线图
			this.k.mts();      //绘制标记图
			this.l.load();     //绘制成交量
			this.MACD.load();  //绘制MACD
			this.touch.load(); //监听
			c.call(o);
		},
		reload:function(){
			this.dispose();//先处理数据
			this.k.load();     //绘制K线图
			this.k.mts();      //绘制标记图
			this.l.load();     //绘制成交量
			this.MACD.load();  //绘制MACD
		},
		k:{
			width:0,
			height:0,
			define:{max:0,min:0,differ:0},//自定义变量(最高价格)
			id:null,
			load:function(){
				var data=stock_dayK.pagedata;
				var $this=null,index=0,price=0;
				if(data==null){return false;}
				this.id=stock_dayK.name+"-k";
				
				var cans=document.getElementById(this.id).getContext('2d');
				this.width=$("#"+this.id).width()*2;
				this.height=$("#"+this.id).height()*2;
				
				$("#"+this.id).attr("width",this.width);
				$("#"+this.id).attr("height",this.height);
				
				//每个间隔
				var interval=this.width/(data.length);
				stock_dayK.define.interval=interval;
				
				cans.beginPath();
				cans.translate(0.5,0.5);
				cans.lineWidth = 1;  
	
				cans.strokeStyle=stock.color.border;
				//cans.strokeStyle="rgba(0,0,0,.3)";
				
				//画框
				cans.strokeRect(0, 0, this.width-1,this.height-1);
				//内框线
				cans.strokeStyle=stock.color.withinBorder;
			
				//价格线
				index=0;
				for(var i=1;i<8;i++){
					if(i%2==1){
						cans.moveTo(0,parseInt(this.height/8*i));
						cans.lineTo(this.width,parseInt(this.height/8*i));
						
						//当前价格
						price=stock.addNum(this.define.max,-this.define.differ/8*i);
						//价格
						$this=$("#"+stock_dayK.name).find(".price li").eq(index);
						$this.css({top:parseInt(this.height/8*i/2)});
						$this.html(price.toFixed(2));
						index++;
					}
				}
				cans.stroke();
				
				//开盘 收盘 最高 最低
				stock_dayK.xy=[];
				var x1=interval/12,y1=0,w1=interval/6*5,h1=0;
				var x2=0,y2=0,w2=0,h2=0;
				var x=0,y=0;  z=0;
				if(stock_dayK.define.pagesize>=200){
					 w2=1;
				}else{
					 w2=2;
				}

				var _xsize=parseInt(stock_dayK.define.pagesize/3);
				var _rightsize=stock_dayK.define.rightsize%_xsize;
				
				//隐藏所有时间
				$("#"+stock_dayK.name).find(".KL-time li").hide();
				for(var i=0;i<data.length;i++){
					var _data=data[i];
					if(_data!=""){
						_data=_data.split(",");
						
						//涨跌平状态 0平 1涨 2跌
						if(_data[11]==2 || _data[11]==0){
							cans.fillStyle=stock.color.green;
							y1=this.getY(_data[1]);
							h1=this.getY(_data[2])-y1;
							y=y1+h1;
						}else{
							cans.fillStyle=stock.color.red;
							y1=this.getY(_data[2]);
							h1=this.getY(_data[1])-y1;
							y=y1;
						}
							
						if(h1<w2){h1=w2;}
						x2=x1+w1/2-w2/2;
						y2=this.getY(_data[3]);
						h2=this.getY(_data[4])-y2;
						x=x2+w2;
						
						//缠论笔标记
						var csr = '';  var bi=0; var bz=0;
						if(stock.clbis.indexOf(_data[0]+",") >= 0){
							csr=stock.clbis.substring(stock.clbis.indexOf(_data[0]+','),stock.clbis.indexOf(_data[0]+';'));
							bi=1;
							if(csr.split(',')[3] == 1){
								bz=this.getY(csr.split(',')[1]);
							}else{
								bz=this.getY(csr.split(',')[2]);
							}
						}
						//趋势的支撑阻力点标记
						var tsps=0; var trts=0; trs=0;
						if(stock.tsps.indexOf(_data[0]+";") >= 0){  //support
							tsps=1; 
							if(_data[11]==2 || _data[11]==0){
								trs=y1+h1;
							}else{
								trs=y1;
							}
						}
						if(stock.trts.indexOf(_data[0]+";") >= 0){ //resistance
							trts=1; 
							if(_data[11]==2 || _data[11]==0){
								trs=y1;
							}else{
								trs=y1+h1;
							}
						}
						
						
						//标记点数据坐标转换, d=0跌 d=1涨  z影线的相对坐标  
						if(_data[11]==2 || _data[11]==0){
							z=y2+h2; d=0; 
						}else{
							z=y2; d=1;
						}
						//tag是否存在macd标记点、bis是否存在笔标记点
						//x, y, z相对纵坐标 
						if(stock.macds.indexOf(_data[0]+";") >= 0){
							stock_dayK.xy.push({x:x,y:y,z:z, d:d,tag:1, bis:bi,biz:bz, sps:tsps,rts:trts,trs:trs });	 
						}else{
							stock_dayK.xy.push({x:x,y:y,z:z, d:d,tag:0, bis:bi,biz:bz, sps:tsps,rts:trts,trs:trs });	
						}
						
						//时间
						for(var j=0;j<4;j++){
							if(x<67){break;}
							if(x>this.width-67){break;}
							if(i==_xsize*j+_rightsize){
								//cans.moveTo(x,0);
								//cans.lineTo(x,this.height);
								$("#"+stock_dayK.name).find(".KL-time li").eq(j).css({left:x/2,display:"block"});
								$("#"+stock_dayK.name).find(".KL-time li").eq(j).html(_data[0]);
							}
						}
						
						cans.fillRect(x1,y1,w1,h1);  //柱子
						cans.fillRect(x2,y2,w2,h2);  //影线
						
						//K线融合
						if(stock.cstrs.indexOf(_data[0]+",") >= 0){
							csr=stock.cstrs.substring(stock.cstrs.indexOf(_data[0]+','),stock.cstrs.indexOf(_data[0]+';'));
							
							cans.strokeStyle=stock.color.purple;
							h2= h2+ (this.getY(parseFloat(csr.split(',')[2])) - (y2+h2));
							//cans.rect(x1,y1+h1,w1,((y2+h2)-(y1+h1)) );
							cans.rect(x1-1,y2+h2,w1+2,2);
							
							y2= this.getY(parseFloat(csr.split(',')[1]));
							//cans.rect(x1,y2,w1,y1-y2);
							cans.rect(x1-1,y2-1,w1+2,2);
								
							/* 
							//上下边框
							if(d==1){ //buy
								//y2= this.getY(parseFloat(_data[3])+parseFloat('1.22'));
								cans.rect(x1,y2,w1,y1-y2);
							}
							if(d==0){
								//h2= h2+ (this.getY(parseFloat(_data[4])-parseFloat('1.22')) - (y2+h2));
								cans.rect(x1,y1+h1,w1,((y2+h2)-(y1+h1)) ); 
							} */
						}
						
						//console.log(i+" ["+x1+"-"+y1+"] ["+w1+"-"+h1+"] {"+x2+"-"+y2+"} {"+w2+"-"+h2+"}");
						x1+=interval;
					}
				}
				cans.globalCompositeOperation="destination-over";
				cans.stroke();
				cans.closePath();
				
				//5日均线:10
				var m5 = 10; //下标10 ma5
				var html="";
				cans.beginPath();
				cans.lineWidth = 1;  
				cans.strokeStyle=eval("stock.color.avg._5");
				cans.moveTo(stock_dayK.xy[0].x,this.getY(data[0].split(",")[m5]));
				for(var i=1;i<data.length;i++){
					cans.lineTo(stock_dayK.xy[i].x,this.getY(data[i].split(",")[m5]));
				}
				cans.stroke();
				cans.save();
				//释放内存
				$this=null,index=null,price=null;  cans=null;
			},
			//自定义业务标记点的绘制
			mts:function(){
				var data=stock_dayK.xy;
				if(data==null){return false;}
				var cans=document.getElementById(stock_dayK.name+"-k").getContext('2d');
				
				//绘制标记点
				for(var i=0;i<data.length;i++){
					//Macd标记点
					if(data[i].tag ==1){
						if(data[i].d==0){
							cans.fillStyle=stock.color.red;
							/*
							cans.beginPath();
							cans.arc(data[i].x,data[i].z+15,10,0,Math.PI*2,true); 
							cans.closePath();
							cans.fill(); */
							cans.beginPath();
							cans.arc(data[i].x,this.height-10,6,0,Math.PI*2,true); 
							cans.closePath();
							cans.fill();
								
							cans.fillStyle=stock.color.green;
						}else{
							cans.fillStyle=stock.color.green;
							/*
							cans.beginPath();
							cans.arc(data[i].x,data[i].z-15,10,0,Math.PI*2,true);
							cans.closePath();
							cans.fill(); */
							cans.beginPath();
							cans.arc(data[i].x,this.height-10,6,0,Math.PI*2,true); 
							//cans.closePath();
							//cans.fill();
							cans.stroke();
								
							cans.fillStyle=stock.color.red;
						}
					}
					//趋势支撑线
					if(data[i].sps ==1){
						//console.log(data[i].sps);
						if(data[i].d==0){
							cans.fillStyle=stock.color.red;
							cans.beginPath();
							cans.arc(data[i].x,data[i].z+15,5,0,Math.PI*2,true); 
							cans.closePath();
							cans.fill(); 
								
							cans.fillStyle=stock.color.green;
						}else{
							cans.fillStyle=stock.color.green;
							cans.beginPath();
							cans.arc(data[i].x,data[i].z-15,5,0,Math.PI*2,true);
							cans.closePath();
							cans.fill(); 
								
							cans.fillStyle=stock.color.red;
						}
					}
				}
				cans.globalCompositeOperation="destination-over";
				cans.stroke();
				cans.closePath();
				
				
				//笔连线（K线图表）
				cans.beginPath();
				cans.lineWidth = 2;
				cans.strokeStyle=stock.color.mts;
				for(var i=1;i<data.length;i++){
					//笔线段
					if(data[i].bis ==1){
						if(data[i].d==0){
							cans.lineTo(data[i].x, data[i].biz);
						}else{
							cans.lineTo(data[i].x, data[i].biz);
						}
					}
				}
				cans.stroke();
				cans.closePath();
				
				//支撑线（K线图表）
				cans.beginPath();
				cans.lineWidth = 2;
				cans.strokeStyle=stock.color.red;
				for(var i=1;i<data.length;i++){
					if(data[i].sps ==1){  //support
						if(data[i].d==0){
							cans.moveTo(data[i].x,data[i].trs);
							cans.lineTo(data[data.length-2].x, data[i].trs);
						}else{
							cans.moveTo(data[i].x,data[i].trs);
							cans.lineTo(data[data.length-2].x, data[i].trs);
						}
					}
				}
				cans.stroke();
				cans.closePath();
				//阻力线（K线图表）
				cans.beginPath();
				cans.lineWidth = 2;
				cans.strokeStyle=stock.color.green;
				for(var i=1;i<data.length;i++){
					if(data[i].rts ==1){ //resistance
						if(data[i].d==0){
							cans.moveTo(data[i].x,data[i].trs);
							cans.lineTo(data[data.length-2].x, data[i].trs);
						}else{
							cans.moveTo(data[i].x,data[i].trs);
							cans.lineTo(data[data.length-2].x, data[i].trs);
						}
					}
				}
				cans.stroke();
				cans.closePath();
				
				
				//笔的标记点连直线（用于分段统计）
				cans.beginPath();
				cans.lineWidth = 2;
				cans.strokeStyle=stock.color.black;
				for(var i=1;i<data.length;i++){
					if(data[i].bis ==1){
						if(data[i].d==0){
							cans.lineTo(data[i].x, this.height-25);
						}else{
							cans.lineTo(data[i].x, this.height-25);
						}
						cans.arc(data[i].x,this.height-25,3,0,Math.PI*2,true); 
						cans.fill();
						if(i<data.length-1){
							cans.font = "normal 22px Verdana";
							cans.fillStyle = "#000000";
							cans.textAlign = "center";
							//cans.fillText(data[i].bis, data[i].x+20,this.height-28);
						}
					}
				}
				cans.stroke();
				cans.closePath();
				
				cans.save();
				//释放内存
				cans=null;
			},
			//Y坐标
			getY:function(price){
				return (this.define.max-price)/this.define.differ*this.height;
			}
		},
		//L线
		l:{
			width:0,
			height:0,	
			define:{max:0,allsize:0},//自定义变量(成交量)
			id:null,
			load:function(){
				var data=stock_dayK.pagedata;
				var $this=null,index=0,price=0;
				if(data==null){return false;}
				this.id=stock_dayK.name+"-l";
				
				var cans=document.getElementById(this.id).getContext('2d');
				
				this.width=parseInt($("#"+this.id).width())*2;
				this.height=parseInt($("#"+this.id).height())*2;

				$("#"+this.id).attr("width",this.width);
				$("#"+this.id).attr("height",this.height);
				
				cans.beginPath();
				cans.translate(0.5,0.5);
				cans.lineWidth = 1;  
				cans.strokeStyle=stock.color.border;
				
				//画框
				cans.strokeRect(0, 0, this.width-1,this.height-1);
				cans.stroke();
				cans.closePath();
				
				cans.beginPath();
				var interval=this.width/(data.length);
				var x=0,y=0,w=interval/6*5,h=0;
				if(this.define.max*1>100000000){
					$("#"+stock_dayK.name).find(".max-volume").html((this.define.max/100000000).toFixed(2)+"亿手");
				}else{
					$("#"+stock_dayK.name).find(".max-volume").html((this.define.max/1000000).toFixed(2)+"万手");
				}
				
				for(var i=0;i<data.length;i++){
					var _data=data[i];
					if(_data!=""){
						_data=_data.split(",");
						
						//跌
						if(_data[1]>_data[2]){
							cans.fillStyle=stock.color.green;
						}else{
							cans.fillStyle=stock.color.red;
						}
						h=(_data[5]/this.define.max)*this.height*0.98;
						x=stock_dayK.xy[i].x-w/2;
						if(h!=0){
							y=this.height-h;
							cans.fillRect(x,y,w,h);
						}
					}
				}
				cans.closePath();
				
				cans=null;  data=null;
			}
		},
		//处理数据
		dispose:function(){
			var data=this.data;
			var _data,define,pagedata=[];
			var price_max=0,price_min=100000000,volume_max=0,volume_count=0;
			var length=data.length;
			
			if(length<this.define.pagesize){
				this.define.pagesize=length;
			}
			var endsize=length-this.define.rightsize;
			var ofset=endsize-this.define.pagesize;	
			
			this.define.ofset=ofset;
			this.define.endsize=endsize;
			//console.log(this.define);
			for(var i=ofset;i<endsize;i++){
				_data=data[i].split(",");
				if(parseFloat(_data[3])>price_max){price_max=_data[3];}
				if(parseFloat(_data[4])<price_min){price_min=_data[4];}
				if(parseFloat(_data[5])>volume_max){volume_max=_data[5];}
				volume_count+=parseFloat(_data[5]/100000000);
				_data=_data.join(",");
				pagedata.push(_data);
			}

			price_max*=1;  price_min*=1;
			price_max+=(price_max-price_min)/11;
			price_min-=(price_max-price_min)/11;
			define={
				max:price_max.toFixed(2),
				min:price_min.toFixed(2),
				differ:(price_max-price_min).toFixed(2)
			}
		
			//赋值
			this.k.define=define;
			define={
				max:volume_max,
				allsize:volume_count
			}
			this.l.define=define;
			this.pagedata=pagedata;
			//释放内存
			delete data; delete pagedata;
			_data=null;  length=null; define=null;
			price_max=null,price_min=null,volume_max=null,volume_count=null;
	},
	//MACD
	MACD:{
		data:null,
		pagedata:new Array(),
		dispose:function(){
			this.pagedata=new Array();
			var data=stock_dayK.pagedata;
			
			var _max=0,_min=100000000;
			var _max2=0,_min2=100000000;
			var endsize=stock_dayK.define.endsize;
			var ofset=stock_dayK.define.ofset;	
			//for(var i=ofset;i<endsize;i++){
			for(var i=0;i<data.length;i++){
				_data=data[i].split(",");
				if(parseFloat(_data[7])>_max){_max=_data[7];}
				if(parseFloat(_data[7])<_min){_min=_data[7];}
				
				if(parseFloat(_data[8])>_max2){_max2=_data[8];}
				if(parseFloat(_data[9])>_max2){_max2=_data[9];}
				
				if(parseFloat(_data[8])<_min2){_min2=_data[8];}
				if(parseFloat(_data[9])<_min2){_min2=_data[9];}
				this.pagedata.push(data[i]);
			}
		
			_max*=13/11;  _min*=13/11;
			this.define.max=_max.toFixed(2);
			this.define.min=_min.toFixed(2);
			if(_max<=0){
				this.define.differ=-_min;
			}else if(_min<=0){
				//max大于0
				this.define.differ=_max*1-_min*1;
			}else{
				//max,min都大于0
				this.define.differ=_max;	
			}
			
			this.define2.max=_max2;
			this.define2.min=_min2;
			if(_max2<=0){
				this.define2.differ=-_min2;
			}else{
				//max大于0
				this.define2.differ=_max2-_min2;
			}
			delete data;
		},
		width:0,
		height:0,
		define:{max:0,min:0,differ:0},//differ 总价格刻度
		define2:{max:0,min:0,differ:0,maxh:0},//differ 总价格刻度
		id:null,
		load:function(){
			this.dispose();
				
			var data=this.pagedata;
			var $this=null,index=0,price=0;
			if(data==null){return false;}
			this.id=stock_dayK.name+"-m";
				
			var cans=document.getElementById(this.id).getContext('2d');
				
			this.width=parseInt($("#"+this.id).width())*2;
			this.height=parseInt($("#"+this.id).height())*2;

			$("#"+this.id).attr("width",this.width);
			$("#"+this.id).attr("height",this.height);
				
			var interval=this.width/(stock_dayK.pagedata.length);
			var x=0,y=0,w=interval/6*5,h=0;
				
			cans.beginPath();
			cans.translate(0.5,0.5);
			cans.lineWidth = 1;  
	
			cans.strokeStyle=stock.color.border;
				
			//画框
			cans.strokeRect(0, 0, this.width-1,this.height-1);
				
			var _maxh=this.define.max/this.define.differ*this.height;		
			
			var $this=null;
				
			$this=$("#"+stock_dayK.name).find(".macds li").eq(0);
			$this.css({top:0})
			$this.html(this.define.max);
				
			$this=$("#"+stock_dayK.name).find(".macds li").eq(1);
			$this.css({top:"50%",marginTop:"-7px"})
			$this.html("0");
				
			$this=$("#"+stock_dayK.name).find(".macds li").eq(2);
			$this.css({bottom:0})
			$this.html(this.define.min);
				
			cans.stroke();
			cans.closePath();
				
			//MACD
			for(var i=0;i<data.length;i++){
				//跌
				if(data[i].split(",")[7]<0){
					cans.fillStyle=stock.color.green;
				}else{
					cans.fillStyle=stock.color.red;
				}
					
				h=data[i].split(",")[7]/this.define.max*_maxh;
				y=_maxh-h;
				x=stock_dayK.xy[i].x-w/2;
				if(h!=0){
					cans.fillRect(x,y,w,h);
				}
			}
			cans.stroke();
			cans.closePath(); 	
			this.define.maxh=this.define.max/this.define.differ*this.height;
			this.define2.maxh=this.define2.max/this.define2.differ*this.height;
				
			//DIF
			cans.beginPath();
			cans.lineWidth = 2;  
			cans.strokeStyle=stock.color.MACD.DIF;
			cans.moveTo(0,this.getY2(data[0].split(",")[8]));
			for(var i=1;i<data.length;i++){
				x=stock_dayK.xy[i].x;
				y=this.getY2(data[i].split(",")[8]);
				cans.lineTo(x,y);
			}
			cans.stroke();
			cans.closePath();
				
			//DEA
			cans.beginPath();
			cans.lineWidth = 2;  
			cans.strokeStyle=stock.color.MACD.DEA;
			cans.moveTo(0,this.getY2(data[0].split(",")[9]));
			for(var i=1;i<data.length;i++){
				x=stock_dayK.xy[i].x;
				y=this.getY2(data[i].split(",")[9]);
				cans.lineTo(x,y);
			}
			cans.stroke();
			cans.closePath();
			
			//MACD三角形态
			cans.strokeStyle='black';
			for(var i=0;i<data.length;i++){
				x=stock_dayK.xy[i].x;
				y=this.getY2(data[i].split(",")[8]);
				if(stock.sinas.indexOf(data[i].split(",")[0]+";") >= 0){
					cans.beginPath();
					cans.arc(x,y,6,0,Math.PI*2,true); 
					cans.stroke();
				}
			}
			cans.save();
			cans=null;  delete data;
		},
		getY2:function(price){
			//return this.define2.maxh-price/this.define2.max*this.define2.maxh;
			return this.define.maxh-(price/this.define.max*this.define.maxh);
		}
	},
	
	//监听事件
	touch:{
		map:null,
		spirit:null,
		size:0,
		_size:0,
		x:0,//用于移动偏离
		x2:0,//由于手机是否长点击
		load:function(){
			var touchID=$("#"+stock_dayK.name).find(".touch").attr("id");//当前ID
			this.map=document.getElementById(touchID);
			var lock1=true;//经过画布时
			var lock2=true;//点击时
			
			//放大缩小
			$("#"+stock_dayK.name).find(".zoom span").click(function(){
				var type=$(this).attr("class");
				var index=10;
				var length=stock_dayK.define.flexsize.length;
				for(var i=0;i<length;i++){
					if(stock_dayK.define.flexsize[i]>=stock_dayK.define.pagesize){
						index=i; break;
					}
				}
				//放大
				if(type=="up"){ index--; }else{ index++; }
				if(index<0){return false;}
				if(index==length){return false;}
				if(stock_dayK.define.flexsize[index] >= stock_dayK.data.length){return false;}
				//console.log(index+' - '+stock_dayK.define.flexsize[index]);
				stock_dayK.define.pagesize=stock_dayK.define.flexsize[index];
				stock_dayK.touch.hideXY();
				lock1=false;
				stock_dayK.reload();
			});
			
			if(stock.isPC){
				//鼠标经过
				$("#"+touchID).hover(function(){
					stock_dayK.touch.showXY(); lock1=true;
				},function(){
					stock_dayK.touch.hideXY(); lock1=false;
				});
				$("#"+touchID).find(".zoom").hover(function(){
					stock_dayK.touch.hideXY(); lock1=false;
				},function(){
					stock_dayK.touch.showXY(); lock1=true;
				});
				$("#"+stock_dayK.name).mousemove(function(e){
					if(lock1){
						e.clientX-=stock.touch.left;
						e.clientY-=stock.touch.top;
						stock_dayK.touch.handle(e.clientX,e.clientY);
					}
				});
			
				$("#"+touchID).mousedown(function(e){
					stock_dayK.touch.touchStart(e.clientX,e.clientY);
					lock2=true;
					$("#"+touchID).addClass("pointer");
					stock_dayK.touch.hideXY();
					lock1=false;
					document.onmousemove = function(e) {
						if(lock2){ 
							stock_dayK.touch.touchMove(e.clientX,e.clientY);
						}
					}
					document.onmouseup = function (e) {
						stock_dayK.touch.touchEnd(e.clientX,e.clientY);
						stock_dayK.touch.showXY();
						lock1=true; lock2=false;
						$("#"+touchID).removeClass("pointer");
					}
				});
			}else{
				var x=0,y=0;
				function touchStart(event) {
					//阻止网页默认动作（即网页滚动）
					event.preventDefault();	
					if (!event.touches.length) return;
					var touch = event.touches[0];
					
					x=touch.pageX; y=touch.pageY;
					stock_dayK.touch.x2=x;
					stock_dayK.touch.touchStart(x,y);
					
					stock_dayK.touch.hideXY();
					lock1=true; lock2=true;
					window.setTimeout(function(){
						if(stock_dayK.touch.x2==x&&lock1){
							stock_dayK.touch.showXY();
							lock2=false;
							stock_dayK.touch.handle(x-stock.touch.left,y-stock.touch.top);	
						}
					},500)
				}
				function touchMove(event) {
					event.preventDefault();
					if (!event.touches.length) return;
					var touch = event.touches[0];
					x=touch.pageX; y=touch.pageY;
					if(stock_dayK.touch.x2!=x){
						lock1=false;
					}
					stock_dayK.touch.x2=x;
					//没有锁定
					if(lock2){
						stock_dayK.touch.touchMove(x,y);
					}else{
						stock_dayK.touch.handle(x-stock.touch.left,y-stock.touch.top);	
					}
				}
				function touchEnd(event) {
					if(lock2){
						stock_dayK.touch.touchEnd(x,y);
					}
					lock1=false;
					stock_dayK.touch.hideXY();
				}
				this.map.addEventListener("touchstart", touchStart, false);
				this.map.addEventListener("touchmove", touchMove, false);
				this.map.addEventListener("touchend", touchEnd, false);	 
			}
		},
		handle:function(x,y){
			var index=Math.round(stock_dayK.pagedata.length/stock_dayK.k.width*x*2);
			var data=stock_dayK.pagedata[index];
			if(data==undefined){
				this.hideXY(); return false;
			}
			data=data.split(",");
			y=stock_dayK.xy[index].y/2;
			
			$("#"+stock_dayK.name).find(".x").css({top:y});
			$("#"+stock_dayK.name).find(".x").find(".current-price").html(data[2]);
		
			x=(stock_dayK.xy[index].x/2)-1;
			if(index==0){x=0;}
			$("#"+stock_dayK.name).find(".y").css({left:x});
			
			//开盘 收盘 最高 最低
			var str="";
			str+='<li class="k-li"><span class="">'+stock.stockName+' '+stock.stockID+'</span></li>';
			str+='<li class="k-li">时间: <span class="grey">'+data[0]+'</span></li>';
			str+='<li class="k-li">开: <span class="">'+data[1]+'</span></li>';
			str+='<li class="k-li">高: <span class="red">'+data[3]+'</span></li>';
			str+='<li class="k-li">低: <span class="green">'+data[4]+'</span></li>';
			str+='<li class="k-li">收: <span class="">'+data[2]+'</span></li>';
			str+='<li style="color:'+eval("stock.color.avg._5")+';">涨跌: <span class="">'+data[12]+'%</span></li>';
			
			str+='<li class="k-li">量: <span class="">'+data[5]+'</span></li>';
			str+='<li style="color:'+eval("stock.color.avg._5")+';">MA5：'+data[10]+'</li>';
			$("#"+stock_dayK.name).find("ul.title").html(str);
			if(true){ 
				data=stock_dayK.MACD.pagedata[index];
				for(var i=0;i<3;i++){
					$("#"+stock_dayK.name).find(".MACD li:eq("+i+") span").html(data.split(",")[7+i]);
				}
			}
			data=null;
		},
		//显示XY
		showXY:function(){
			$("#"+stock_dayK.name).find(".x").show();
			$("#"+stock_dayK.name).find(".y").show();
		},
		//隐藏XY
		hideXY:function(){
			$("#"+stock_dayK.name).find(".x").hide();
			$("#"+stock_dayK.name).find(".y").hide();
		},
		touchStart:function(x,y){
			x-=stock.touch.left;
			y-=stock.touch.top;
			this.x=x;
			this.size=stock_dayK.define.rightsize;
		},
		touchMove:function(x,y){
			x-=stock.touch.left;
			y-=stock.touch.top;
			if(x<0){x=0;}
			if(y<0){y=0;}
			//每次移动格数
			var _size=parseInt((x-this.x)/stock_dayK.define.interval*2);
			//清除多余
			if(this._size==_size){ return false; }
			this._size=_size;
			
			stock_dayK.define.rightsize=_size+this.size;
			if(stock_dayK.define.rightsize<0){
				stock_dayK.define.rightsize=0;
				return false;
			}
			if(stock_dayK.define.rightsize+stock_dayK.define.pagesize>=stock_dayK.data.length){
				stock_dayK.define.rightsize=stock_dayK.data.length-stock_dayK.define.pagesize;
				return false;
			}
			stock_dayK.reload();		
		},
		touchEnd:function(x,y){
			var _size=parseInt((x-this.x)/stock_dayK.define.interval*2);
			stock_dayK.define.rightsize=this.size+_size;
			if(stock_dayK.define.rightsize<0){
				stock_dayK.define.rightsize=0;
				return false;
			}
			if(stock_dayK.define.rightsize+stock_dayK.define.pagesize>=stock_dayK.data.length){
				stock_dayK.define.rightsize=stock_dayK.data.length-stock_dayK.define.pagesize;
				return false;
			}
			this.x=0;
		}
	}		
}
